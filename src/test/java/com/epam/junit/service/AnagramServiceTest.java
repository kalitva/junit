package com.epam.junit.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class AnagramServiceTest {
    private final AnagramService anagramService = new AnagramService();

    @ParameterizedTest
    @MethodSource("provideTextAndMessages")
    void makeAnagram_WhenTextIsNull_ThenShouldThrowIllegalArgumentException(String text, String message, String description) {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> anagramService.makeAnagram(text));
        assertThat(exception.getMessage())
                .withFailMessage(description)
                .isEqualTo(message);
    }

    private static Stream<Arguments> provideTextAndMessages() {
        return Stream.of(
                Arguments.of(null, "Text is null", "makeAnagram() should throw exception if the text is null"),
                Arguments.of("", "Text is empty", "makeAnagram() should throw exception if the text is empty"),
                Arguments.of("  \t", "Text is blank", "makeAnagram() should throw exception if the text is blank")
        );
    }

    @ParameterizedTest
    @MethodSource("provideText")
    void makeAnagram_ShouldReverseText(String text, String expected) {
        assertEquals(expected, anagramService.makeAnagram(text));
    }

    private static Stream<Arguments> provideText() {
        return Stream.of(
                Arguments.of("Hello world", "olleH dlrow"),
                Arguments.of("   Hello \nworld  ", "olleH dlrow"),
                Arguments.of("Hello\tworld  ", "olleH dlrow")
        );
    }
}
