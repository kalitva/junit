package com.epam.junit.controller;

import com.epam.junit.service.AnagramService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@ExtendWith(MockitoExtension.class)
class AnagramControllerTest {
    private MockMvc mockMvc;

    @Mock
    private AnagramService anagramService;

    @InjectMocks
    private AnagramController anagramController;

    @BeforeEach
    void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(anagramController).build();
    }

    @Test
    void homeTest() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("home"));
    }

    @Test
    void anagramTest() throws Exception {
        when(anagramService.makeAnagram("Hello")).thenReturn("olleH");

        mockMvc.perform(post("/make-anagram").param("text", "Hello"))
                .andExpect(status().isOk())
                .andExpect(view().name("anagram"))
                .andExpect(model().attribute("anagram", "olleH"));

        verify(anagramService, times(1)).makeAnagram("Hello");
    }

    @Test
    void exceptionHandlerTest() throws Exception {
        when(anagramService.makeAnagram("")).thenThrow(new IllegalArgumentException("error message"));

        mockMvc.perform(post("/make-anagram").param("text", ""))
                .andExpect(status().isOk())
                .andExpect(view().name("home"))
                .andExpect(model().attributeExists("error"));
    }
}
