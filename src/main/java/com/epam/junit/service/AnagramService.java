package com.epam.junit.service;

import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.stream.Collectors;

@Service
public class AnagramService {
    private static final String WHITESPACE_REGEX = "\\s+";
    private static final String WHITESPACE = " ";

    public String makeAnagram(String text) {
        validate(text);
        return Arrays.stream(text.trim().split(WHITESPACE_REGEX))
                .map(this::reverse)
                .collect(Collectors.joining(WHITESPACE));
    }

    private String reverse(String word) {
        return new StringBuilder(word)
                .reverse()
                .toString();
    }

    private void validate(String text) {
        if (text == null) {
            throw new IllegalArgumentException("Text is null");
        }
        if (text.isEmpty()) {
            throw new IllegalArgumentException("Text is empty");
        }
        if (text.isBlank()) {
            throw new IllegalArgumentException("Text is blank");
        }
    }
}
