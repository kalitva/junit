package com.epam.junit.controller;

import com.epam.junit.service.AnagramService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AnagramController {
    private final AnagramService anagramService;

    public AnagramController(AnagramService anagramService) {
        this.anagramService = anagramService;
    }

    @GetMapping
    public String home() {
        return "home";
    }

    @PostMapping("/make-anagram")
    public String anagram(@RequestParam String text, Model model) {
        model.addAttribute("anagram", anagramService.makeAnagram(text));
        return "anagram";
    }

    @ExceptionHandler(IllegalArgumentException.class)
    private String exceptionHandler(Exception exception, Model model) {
        model.addAttribute("error", exception.getMessage());
        return "home";
    }
}
